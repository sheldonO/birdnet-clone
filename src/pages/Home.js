import React from "react";
import { Footer } from "../components/Footer";
import { ServiceCards } from "../components/ServiceCards";
import "./Home.css";
import { Faqs } from "../components/Faqs";

export const Home = () => {
  const reveal = () => {
    var reveals = document.querySelectorAll(".reveal");

    for (var i = 0; i < reveals.length; i++) {
      var windowHeight = window.innerHeight;
      var elementTop = reveals[i].getBoundingClientRect().top;
      var elementVisible = 150;

      if (elementTop < windowHeight - elementVisible) {
        reveals[i].classList.add("active");
      } else {
        reveals[i].classList.remove("active");
      }
    }
  };

  window.addEventListener("scroll", reveal);

  return (
    <>
      <div className="container home-section">
        <section>
          <span className="intro-chip">Order, pay & get delivery done</span>

          <div className="row hero-section">
            <div className="col-md-6">
              <h6 className="hero-header">You Got Package?</h6>

              <p className="hero-description">
                <strong>Buga </strong> is a logistics company that offers
                affordable delivery. It is the Most Versatile And Efficient
                Logistics Service In Nigeria. Best Drivers, Best Prices, Parcel
                Tracking.
              </p>

              <div className="hero-download-img">
                <img src="./images/ios-hero.svg" />
                <img src="./images/android-hero.svg" />
              </div>
            </div>

            <div className="col-md-6 hero-image-section">
              <img src="./images/Buga.png" />
            </div>
          </div>
        </section>

        <ServiceCards />

        <section className="check-section" id="banking">
          <div className="row">
            <div className="col-md-6 reveal fade-left">
              <h1 className="check-header">
                Quality riders and partners ready to deliver your goods.
              </h1>

              <p className="check-desc-1">
                With divers partnered from all over Lagos, there will always be
                one nearby to pick up your parcels and more.
              </p>

              <p className="check-desc-2">
                Our drivers are tested with our selection process that ensures
                we give you quality drivers.
              </p>
            </div>
            <div className="col-md-6 check-img reveal fade-right">
              <img src="./images/bug-app.png" />
            </div>
          </div>
        </section>

        <section className="entry-section" id="border">
          <div className="row">
            <div className="col-md-6 entry-img reveal fade-left">
              <img src="./images/bug-serve.png" />
            </div>
            <div className="col-md-6 reveal fade-right">
              <h1 className="check-header">Seamless Pickup Requests</h1>

              <p className="check-desc-1">
                Need to have your item picked up at a specific time? We’ve got
                that covered.
              </p>
            </div>
          </div>
        </section>

        <section id="account">
          <div className="row account-section">
            <div className="col-md-6 reveal fade-left">
              <h1 className="check-header">24/7 Messaging Service</h1>

              <p className="check-desc-1">
                The app allows you to communicate with our customer care and
                also the rider.
              </p>

              <div className="account-images">
                <img src="./images/ios-dark.svg" />
                <img src="./images/android-dark.svg" />
              </div>
            </div>
            <div className="col-md-6 account-des reveal fade-right">
              <img src="./images/message.png" />
            </div>
          </div>
        </section>

        <section id="buga-app">
          <div className="row checkout-section">
            <div className="col-md-6 checkout-img reveal fade-left">
              <img
                src="./images/download.png"
                style={{ borderRadius: ".5rem" }}
              />
            </div>
            <div className="col-md-6 checkout-desc reveal fade-right">
              <h1>DOWNLOAD OUR MOBILE APP NOW</h1>

              <p className="check-desc-1">
                Download <strong>Buga App</strong> for best delivery service
              </p>

              <div className="account-images">
                <img src="./images/ios-dark.svg" />
                <img src="./images/android-dark.svg" />
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="container faq-section reveal active">
            <h1 className="text-center">Frequently asked questions</h1>
            <p className="text-center">
              Everything you need to know about the product
            </p>
            <Faqs />
          </div>
        </section>
      </div>

      <Footer />
    </>
  );
};
