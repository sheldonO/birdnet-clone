import { ServiceCard } from "./ServiceCard";
import "./ServiceCards.css";

export const ServiceCards = () => {
  return (
    <section className="service-section" id="features">
      <div className="service-cards reveal active">
        <ServiceCard
          actionImage="./images/send.svg"
          action="Sign Up"
          actionMessage="We have a form for you to fill in the app."
        />

        <ServiceCard
          actionImage="./images/spend.svg"
          action="Order"
          actionMessage="Make a service order with an easy location for seamless
                  service."
        />

        <ServiceCard
          actionImage="./images/save.svg"
          action="Driver"
          actionMessage="Get drivers who are tested with our selection process
                  that ensures we give you quality drivers."
        />

        <ServiceCard
          actionImage="./images/invest.svg"
          action="Invoice"
          actionMessage="After an order service, you will receive an invoice."
        />
      </div>
    </section>
  );
};
