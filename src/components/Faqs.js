import { Faq } from "./Faq"
import "./Faq.css"
export const Faqs = () => {

    return (
      <>
        <div className="faq-l">
          <Faq
            question="What is Buga?"
            answer="Buga is a logistics company that provides seamless delivery services."
          />

          <Faq
            question="App installation failed, how to update system information?"
            answer="Buga app has a 24 hr message communication with our customer care which you are free to communicate with us."
          />

          <Faq
            question="Website response taking time, how to improve?"
            answer="Product name allows you to open a US checking account in dollars, a wealth
              building account, make cross-border and P2P transfers in various
              currencies, manage multiple accounts from single app, paybills and
              provides a checkout solution for apps and websites."
          />

          <Faq
            question="Didn't receive your delivery?"
            answer="Product name allows you to open a US checking account in dollars, a wealth
              building account, make cross-border and P2P transfers in various
              currencies, manage multiple accounts from single app, paybills and
              provides a checkout solution for apps and websites."
          />
        </div>
      </>
    );
}