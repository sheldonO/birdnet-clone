import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./NavBar.css";

function NavBar() {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const [scroll, setScroll] = useState(0);

  window.addEventListener("scroll", () => {
    if (window.scrollY > 0) {
      setScroll(1);
    } else {
      setScroll(0);
    }
  });

  return (
    <>
      <nav
        style={{
          boxShadow: scroll >= 1 ? "0 2px 4px 0 rgba(0,0,0,.2)" : "none",
        }}
      >
        <div className=" container nav-items">
          <div className=" nav-section">
            <Link to="/">
              <img
                src="./images/logo-yellow.png"
                style={{ width: "3rem", marginTop: "1rem" }}
              />
            </Link>
            <div>
              <ul className={click ? "links active" : "links"}>
                <li className="product">
                  <a href="#features">Features</a>
                </li>

                <li>
                  <a href="#buga-app" onClick={closeMobileMenu}>
                    Buga app
                  </a>
                </li>

                <li>
                  <Link to="/contact" onClick={closeMobileMenu}>
                    Contact Us
                  </Link>
                </li>

                <div className="drop-nav-btn">
                  <Link to="/signup">
                    <button>Get started today</button>
                  </Link>
                </div>
              </ul>
            </div>
          </div>

          <div className="nav_btn">
            <Link to="/signup">
              <button>Get started today</button>
            </Link>
          </div>

          <button onClick={handleClick} className="drop-btn">
            <i className={click ? "uil uil-multiply" : " fa fa-bars"}></i>
          </button>
        </div>
      </nav>
    </>
  );
}
export default NavBar;
