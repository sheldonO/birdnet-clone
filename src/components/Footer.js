
import "./Footer.css"

export const Footer = () => {

    return (
      <>
        <section>
          <footer>
            <div className="container footer-section">
              <div className="footer-start">
                <h3>Get the app</h3>
                <div>
                  <img src="./images/ios-light.svg" />
                  <img src="./images/android-light.svg" />
                </div>
              </div>

              <div className="footer-main">
                <div className="footer-section-1">
                  <img src="./images/logo-light.png" style={{ width: "20%" }} />
                  <div>
                    <strong>Buga</strong> is a logistics company that offers
                    affordable delivery. It is the Most Versatile And Efficient
                    Logistics Service In Nigeria. Best Drivers, Best Prices,
                    Parcel Tracking.
                  </div>
                </div>

                <div className="footer-section-2">
                  <h6>App Features</h6>
                  <div>
                    <p>Seamless Pickup Requests</p>
                    <p>24/7 Messaging Service</p>
                    <p>Multi-Lingual Function</p>
                    <p>Detailed Descriptive Option</p>
                  </div>
                </div>

                <div className="footer-section-3">
                  <h6>Buga Sevices</h6>
                  <div>
                    <p>Parcel delivery</p>
                    <p>24 hr cutomer messaging services.</p>
                  </div>
                </div>

                <div className="footer-section-3">
                  <h6>support</h6>
                  <div>
                    <p>Privacy Policy</p>
                    <p>Terms and conditions</p>
                    <p>Contact Us</p>
                  </div>
                </div>
              </div>

              <div className="footer-end">
                <p>
                  2022 <strong>Company name </strong>.All right reserved
                </p>

                <div className="socials">
                  <img src="./images/twitter.svg" />
                  <img src="./images/linkedIn.svg" />
                  <img src="./images/facebook.svg" />
                </div>
              </div>
            </div>
          </footer>
        </section>
      </>
    );
}